/* Author:

*/

$(document).ready(function () {
// Code here
  var params = {
     start: 0
    ,sizes: [1280,960,768,360]
    ,transition: 'crossFade'
    ,autoplay: true
  };

  var params2 = $.extend($.extend({},params), {
    start:1
    ,controls_selector: ".cfs-controls-2"
    ,pager_selector: "#cfs-pager-2"
    ,counter_selector: "#cfs-counter-2"
    ,player_selector: "#cfs-player-2"
    ,autoplay: true
    ,delay: 3000
    ,transition: 'slideUp'
  });

  var slide1 = $(".slider1").cfslider(params);
  var slide2 = $(".slider2").cfslider(params2);
  
});


