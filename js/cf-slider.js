/**
  * Control Freak Slider 
  * @author - sherakama
  * http://cfslider.mckmedia.ca/
  * https://bitbucket.org/sherakama/cfslider
  
  TODO: Add fitVid.js to support video
  TODO: add touch events
  TODO: API Callback function on image change
*/
(function( $ ){

  /**
   * SLIDER CLASS
   * @param - object 
   *    The jquery collection based from the selector
   * @param - object
   *    An object containing additional configuration options 
   **/
 
  function CFSlider(selector, params) {
  
    var _defaults = {
       sizes: [0]                                 // Media query break points
      ,start: 0                                   // The starting index of the slides
      ,counter_seperator: ' of '                  // anything you want the counter to be divided by
      ,pager_label: 'dots'                        // can be dots or numbers or empty | TODO: add images array
      ,transition: 'none'                         // Default transition function
      ,duration: 500                              // Default time it takes to complete transition
      ,autoplay: false                            // Wether or not to autoplay through the slides
      ,delay: 5000                                // Default time between transitions
      ,controls_selector: '#cfs-controls'         // CSS selector for controls container
      ,pager_selector: '#cfs-pager'               // CSS selector for pager container
      ,counter_selector: '#cfs-counter'           // CSS selector for counter container
      ,player_selector: '#cfs-player-controls'    // CSS selector for the player controls
    };

    CFSlider.prototype._config = _defaults;
    CFSlider.prototype._element = null;           // The cfs slider container element
    CFSlider.prototype.me = this;                 // as in the CFSlider function
    CFSlider.prototype._index = 0;                // current image index
    CFSlider.prototype._controls = null;          // Nav Controls collection container
    CFSlider.prototype._player_controls = null;   // Player Controls collection container
    CFSlider.prototype._timer = null;             // Container for the player timer
    
    /**
    * INIT 
    **/
    CFSlider.prototype._init = function() {
      this.set_config($.extend(_defaults,params));
      this._set_slides();
      this._set_starting_slide();
      this._init_responsive_images();
      this._setup_controls();
      this._setup_pager();
      this._setup_counter();
      this._setup_player_controls();
      
      // Start autoplay
      if(this._config.autoplay) {
        this.start_player_timer();
      }
      
      $(window).resize(); // tigger a resize 
    }
    
    
    /**
    * Gets the current configuration
    **/
    CFSlider.prototype.get_config = function() {
      return this._config;
    }
    
    /**
    * Sets a new configuration
    **/
    CFSlider.prototype.set_config = function(config) {
            
      if(config.sizes !== undefined) {
        config.sizes.sort(function(a,b){return b - a});
        config.sizes.push(0);
      } 
            
      this._config = config;
    }
    
    /**
    * Set slides collection
    */
    CFSlider.prototype._set_slides = function(slides) {
      if(slides == null || slides == undefined) {
        this._slides = this._element.find(".cfs-slides").children();
      } else {
        this._slides = slides;
      }
      
      if(this._slides.length == 0) { console.log('CFSlider Error: no slides found'); }
    }
    
    /**
    * Returns the slides collection
    **/
    CFSlider.prototype._get_slides = function() {
      return this._slides;
    }
    
    
    /**
    * Sets the active class on the starting slide
    **/
    CFSlider.prototype._set_starting_slide = function() {
      var slides = this._get_slides();
      var _config = this.get_config();
      this._index = _config.start;
      
      slides.hide(); // hide slides
      slides.eq(_config.start).show().addClass('cfs-active-slide');
    }
    
    /**
    * Gets all the images in this CFslider and returns a jquery collection
    * @return - jquery collection of images
    **/
    CFSlider.prototype._get_images = function() {
      return this._element.find(".cfs-slides img");
    }
    
    
    /**
    * Finds the currently active image by class
    * @return - jquery object of the active image
    **/
    CFSlider.prototype._get_active_image = function() {
      return this._element.find(".cfs-active-slide img");
    }
    
  
    /**
    * Enables the responsive image styling
    **/
    CFSlider.prototype._init_responsive_images = function() {
          
     $(window).resize(function() {
       $.each($.cfslider.instances, function(i, v) {
         v.window_resized();
       });
     });
      
    }
  
    /**
    *
    */
    CFSlider.prototype.window_resized = function(obj) {
      var _config = this.get_config();
      var _element = this._element;
      var winw = $(document).width();
      var me = this;
      var $img = me._get_active_image();

      if(typeof obj !== "undefined" && 'target_img' in obj) {
        $img = obj.target_img;
      }

     if(_config.sizes == undefined) { return false; }
          
     $.each(_config.sizes, function(i,v) {
       
       if(winw > v) {
         var src = $img.attr('data-' + v);
                  
         if(src !== undefined && src.length) {
           $img.attr("src", src);
         }
        
       return false;
       }
       
       // Window is smaller then all the breakpoints
       // Go with the default src 
     });
     
    }
    
    
    /**
    * Setup the navigation controls
    **/
    CFSlider.prototype._setup_controls = function() {
      
      var config = this.get_config();
      
      // Look for controls
      var controls = $(config.controls_selector);
      if(!controls.length) { return; }
      
      // Found controls add click handlers to prev/next
      var prev = controls.find('.cfs-previous');
      
      prev.data('instance', this);
      prev.click(function(e){
        e.preventDefault();
        $(this).data('instance').get_image('prev');
        $(this).data('instance').kill_player_timer();
      });

      // Found controls add click handlers to prev/next
      var next = controls.find('.cfs-next');
      next.data('instance',this);
      next.click(function(e){
        e.preventDefault();
        $(this).data('instance').get_image('next');
        $(this).data('instance').kill_player_timer();
      });
      
    }
    
    
    /**
    * Get image handler
    */
    CFSlider.prototype.get_image = function(what) {
      var from = this._index;
      var to = this._index;
      var config = this.get_config();
      
      if(what == 'next') { to++; }
      else if(what == 'prev') { to--; }
      else if(typeof what == "number" && (what % 1) == 0) { to = what; }
      
      if(to < 0) { to = this._slides.length - 1; }
      if(to >= this._slides.length) { to = 0; }
          
      if(this._slides.eq(to) == undefined) { to = 0; }

      // reset the current index
      this._index = to;

      // Run a transition
      $.cfslider.transitions[config.transition]({
         to : this._slides.eq(to)
        ,from : this._slides.eq(from)
        ,duration : config.duration
        ,instance : this
      });
      
      
      // additionals
      $(window).resize();
      this._set_pager(to);
      this._update_counter();
    }
    
    
    /**
    * Add the pager markup to the slider if there is a pager container available
    */
    CFSlider.prototype._setup_pager = function() {
      var config = this.get_config();
      var pager = $(config.pager_selector);
      if(!pager.length) { return; }
      
      this._pager = pager;
      var slides = this._get_slides();
      var config = this.get_config();
      var pager_markup = $("<ul />").addClass('cfs-pager-container');
      var i = 0;
      
      
      
      while(i < slides.length) {
        var li = {};
        
        // Bullet labels
        if(config.pager_label == 'dots') {
          li = $("<li />").append('<span class="cfs-pitem-' + i + '">&bull;</span>');
        }
       
        // Number labels
        if(config.pager_label == 'number') {
          li = $("<li />").append('<span class="cfs-pitem-' + i + '">' + (i+1) + '</span>');
        }
        
        if(config.pager_label == "empty") {
          li = $("<li />").append('<span class="cfs-pitem-' + i + '"></span>');          
        }
       
       // active pager
       if(i == this._index) { li.addClass('cfs-active-pager'); }
       li.data('slide-index',i);
       li.data('instance',this);
       
       // Click event handling
       li.click(function(e){
         var instance = $(this).data('instance');
         var index = $(this).data('slide-index');
         instance.get_image(index);
         instance.kill_player_timer();
       });
       
       // Append markup to <ul>
       pager_markup.append(li);
       
       i++; 
      }
    
    pager.append(pager_markup);
    
    }
    
    /**
    * changes the active pager element
    */
    CFSlider.prototype._set_pager = function(to) {
      if(this._pager == undefined) { return; }
      this._pager.find('.cfs-active-pager').removeClass('cfs-active-pager');
      this._pager.find("li").eq(to).addClass('cfs-active-pager');
    }
    
    
    /**
    * Add counter markup
    */
    CFSlider.prototype._setup_counter = function() {
      var config = this.get_config();
      var counter = $(config.counter_selector);
            
      if(!counter.length) { return; }
      
      this._counter = counter;
      var current = this._index + 1;
      var total = this._get_slides().length;
      
      this._counter.append($("<span />").addClass('cfs-counter-index').text(current));
      this._counter.append($("<span />").addClass('cfs-counter-seperator').html(config.counter_seperator));
      this._counter.append($("<span />").addClass('cfs-counter-total').text(total));
        
    }
    
    /**
    * Add counter markup
    */
    CFSlider.prototype._update_counter = function(to) {
      if(this._counter == undefined) { return; }
      var current = to || this._index + 1;
      
      this._counter.find('.cfs-counter-index').text(current);      
    }
    
    /**
    * Add event handling to the player controls and set the text of the playpause button
    *
    **/
    
    CFSlider.prototype._setup_player_controls = function() {
      var config = this.get_config();
      var player_controls = $(config.player_selector);
            
      if(player_controls.length <= 0) { return; }
      
      this._player_controls = player_controls;
      player_controls.data('instance',this);
      
      var button = player_controls.find('.cfs-play-pause');
      button.data('instance',this);
      
      if(config.autoplay) {
        button.addClass('paused').text('pause');
      } 
      else {
        button.addClass('play').text('play');
      }
      
      button.click(function(e){
        e.preventDefault();
        var me = $(this);
        me.toggleClass('play','paused');
        if(me.hasClass('play')) {
          me.data('instance').kill_player_timer();
        }
        else {
          me.text('pause');
          me.data('instance').start_player_timer();
        }
      });
      
      
    }
    
    /**
    * Create a timer to automatically advance through a gallery
    *
    **/
    
    CFSlider.prototype.start_player_timer = function() {
      var config = this.get_config();
      var self = this;
      this._timer = setInterval(function() { self.get_image('next'); }, config.delay);
    }
    
    
    /**
    * Kill the autoplay through timer
    **/
    
    CFSlider.prototype.kill_player_timer = function() {
      clearInterval(this._timer);
      this._player_controls.find('.cfs-play-pause')
        .removeClass('pause')
        .addClass('play')
        .text('play');
    }
    
    
    /**
    * A couple of configuration items
    */
    
    this._element = selector;
    this._element.data('instance', this); 
    
    // GO!
    this._init();    
    
  } // end CFSLIDER Class

  /**
  * Add a global config istance to jquery object
  **/

  $.extend($, { cfslider : {
     instances : []
    ,transitions : {
      
      /** 
      * No transitions
      */ 
      none : function(opts) {
        opts.to.show().addClass('cfs-active-slide');
        opts.from.hide().removeClass('cfs-active-slide');
      }
      /** 
      * Fade active out and new one in
      */ 
      ,fade : function(opts) {
        
        // Adjust src of the next image
        opts.instance.window_resized({target_img:opts.to.find("img:first")});
        
        opts.from
          .fadeOut(opts.duration,function(e){
            opts.from.removeClass('cfs-active-slide');
            opts.to
              .fadeIn(opts.duration,function(e){
                opts.to.addClass('cfs-active-slide');
              });
          });
      }
      /**
      * CrossFade Between the two images
      */
      ,crossFade : function(opts) {
        
        // Adjust src of the next image
        opts.instance.window_resized({target_img:opts.to.find("img:first")});
                
        opts.from
          .fadeOut(opts.duration,function(e){
            opts.from.removeClass('cfs-active-slide');
          });

        opts.to
          .css({position:"absolute",top:0,left:0})
          .fadeIn(opts.duration,function(e){
            opts.to.css({position:"relative"}).addClass('cfs-active-slide');
          });
          
      }
      /**
      * Slide Active left and out with new one following
      * From the right
      */
      ,slideLeft : function(opts) {
        
        // Adjust src of the next image
        opts.instance.window_resized({target_img:opts.to.find("img:first")});
        
        opts.from
          .css({position:"relative"})
          .animate(
            {
              left:'-100%'
            }
            ,opts.duration
            ,function(e){
              opts.from.hide().css({left:0}).removeClass('cfs-active-slide');
            }
          );
        
        opts.to
          .show()
          .css({position:"absolute",top:0,left:"100%",width:"100%"})
          .animate(
            {left:"0%"}
            ,opts.duration
            ,function(){
              opts.to.css({position:"relative",width:'auto'}).addClass('cfs-active-slide');
            }
          );        
      }
      /**
      * Slide Active right and out with new one following
      * From the right
      */
      ,slideRight : function(opts) {
        
        // Adjust src of the next image
        opts.instance.window_resized({target_img:opts.to.find("img:first")});
        
        opts.from
          .css({position:"relative"})
          .animate(
            {
              left:'100%'
            }
            ,opts.duration
            ,function(e){
              opts.from.hide().css({left:0}).removeClass('cfs-active-slide');
            }
          );
        
        opts.to
          .show()
          .css({position:"absolute",top:0,left:"-100%",width:"100%"})
          .animate(
            {left:"0%"}
            ,opts.duration
            ,function(){
              opts.to.css({position:"relative",width:'auto'}).addClass('cfs-active-slide');
            }
          );
      }
      /**
      * Slide Active right and out with new one following
      * From the right
      */
      ,slideUp : function(opts) {
        var fh = opts.from.height() * -1;
        var th = opts.from.height();
        
        // Adjust src of the next image
        opts.instance.window_resized({target_img:opts.to.find("img:first")});
        
        opts.from
          .css({position:"relative"})
          .animate(
            {
              top: fh
            }
            ,opts.duration
            ,function(e){
              opts.from.hide().css({top:0}).removeClass('cfs-active-slide');
            }
          );
                
        opts.to
          .show()
          .css({position:"absolute",top:th,left:"0",width:"100%"})
          .animate(
            {top: 0}
            ,opts.duration
            ,function(){
              opts.to.css({position:"relative",width:'auto', top: 0}).addClass('cfs-active-slide');
            }
          );
      }
      /**
      * Slide Active right and out with new one following
      * From the right
      */
      ,slideDown : function(opts) {
        var fh = opts.from.height();
        var th = opts.to.height() * (-1);
        var tht = th + fh;
        if(tht > 0) { tht = 0; }
        
        // Adjust src of the next image
        opts.instance.window_resized({target_img:opts.to.find("img:first")});
        
        opts.from
          .css({position:"relative"})
          .animate(
            {
              top: fh
            }
            ,opts.duration
            ,function(e){
              opts.from.hide().css({top:0}).removeClass('cfs-active-slide');
            }
          );
        
        opts.to
          .show()
          .css({position:"absolute",top:th,left:"0",width:"100%"})
          .animate(
            {top: tht}
            ,opts.duration
            ,function(){
              opts.to.css({position:"relative",width:'auto', top: 0}).addClass('cfs-active-slide');
            }
          );
      }
      
      
    } // cfslider transitions obj
  } // extend cfslider obj
  }); // extend

  /**
   * Add to the jquery fn object 
   **/
    $.extend($.fn, {
        cfslider: function(params){
          
          var selector = $(this);
          var slider = new CFSlider(selector, params);
          $.cfslider.instances.push(slider);
          return slider;
        }
    });
    
})( jQuery );